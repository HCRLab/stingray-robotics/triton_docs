<p align="center"> 
  <img src="media/Logo.png" alt="HAR Logo" width="80px" height="80px">
</p>
<h1 align="center"> Triton </h1>
<h3 align="center"> A setup guide for the ultimate educational mobile robot platform </h3>  

</br>

<!-- TABLE OF CONTENTS -->
<h2 id="table-of-contents"> :book: Table of Contents</h2>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-triton"> ➤ About Triton</a></li>
    <li><a href="#prerequisites"> ➤ Prerequisites</a></li>
    <li>
      <a href="#setup"> ➤ Setup</a>
      <ul>
        <li><a href="#hardware">Hardware Setup</a></li>
        <li><a href="#software">Software Setup</a></li>
      </ul>
    </li>
    <li><a href="#poweroff"> ➤ Powering Off</a></li>
  </ol>
</details>

![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

<!-- ABOUT TRITON -->
<h2 id="about-triton"> :robot: About Triton</h2>

<p align="center">
  <img src="media/triton-cover.jpg" alt="Table1: 18 Activities" width="70%" height="70%">        
</p>

<p align="justify"> 
  This manual serves to present the mobile robot, Triton, which has been developed by the <a href="https://hcr.cs.umass.edu/">Human-Centered Robotics Lab</a>. Named after the Greek god of the sea, Triton is specifically designed to function as an educational mobile robot development platform for the <a href="https://hcr.cs.umass.edu/courses/compsci603/">COMPSCI 603 Robotics course</a> at the <a href="https://www.umass.edu/">University of Massachusetts Amherst</a>. 

  The Triton robot embodies exceptional capabilities for facilitating research in the domains of robot learning, perception, and autonomous navigation. Triton is a 3-wheeled omnidirectional robot powered by a micro controller board ([Elegoo MEGA R3 Board ATmega 2560](https://www.elegoo.com/products/elegoo-mega-2560-r3-board)), a powerful computing board with GPU ([NVIDIA Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)), multiple sensors (LiDAR, RGB-D camera), and Robot Operating System.
</p>


![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)


<!-- PREREQUISITES -->
<h2 id="prerequisites"> :fork_and_knife: Prerequisites</h2>

### Docker

  Docker is a software platform that allows you to build, test, and deploy applications quickly. Docker packages software into standardized units called containers that have everything the software needs to run including libraries, system tools, code, and runtime. Using Docker, you can quickly deploy and scale applications into any environment and know your code will run. Its main goal is to solve the issue of "but it runs on my machine".

  Docker works by providing a standard way to run your code. Docker is an operating system for containers. Similar to how a virtual machine virtualizes (removes the need to directly manage) server hardware, containers virtualize the operating system of a server. Docker is installed on each server and provides simple commands you can use to build, start, or stop containers. 

  ROS Noetic on Triton is inside a Docker container. So all of the robot's code will be run using Docker. This mean it will be helpful for you to have some basic knowldege on Docker and Docker commands. You can take a look at existing <a href="https://docker-curriculum.com/">tutorials</a> to familiarize yourself.

### PyTorch

  PyTorch is an open-source Python and C++ machine learning framework widely used for deep learning tasks. It provides a flexible and efficient platform for building, training, and deploying neural networks. PyTorch employs dynamic computational graphs, allowing for easy model creation and modification on-the-fly, making it popular among researchers and developers. Its intuitive interface and extensive support for GPU acceleration enable users to implement complex neural network architectures effortlessly, facilitating rapid experimentation and prototyping in the field of artificial intelligence.

  PyTorch isn't a hard requirement for working with Triton, but you may work on a project that requires some sort of neural network based model/s. In that case, knowing PyTorch is beneficial. You can take a look at existing <a href="https://pytorch.org/tutorials/">tutorials</a> to familiarize yourself.

![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

<!-- SETUP -->
<h2 id="setup"> :hammer: Setup</h2>

<p align="justify"> 
  The guide will help you to setup your hardware and software environment for further development in the Triton robot.
  
  * <b>Hardware Setup</b>: Follow the steps below to build your robot hardware and watch this [video](https://www.youtube.com/watch?v=uPFR7fa0M-g) until minute 9
  * <b>Software Setup</b>: Follow the steps below for Docker and software setup and watch this [video](https://youtu.be/MpRGMVvnzUg)
  
![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

<!-- HARDWARE -->
<h2 id="hardware"> :diamond_shape_with_a_dot_inside: Hardware Setup</h2>
<p align="justify"> 

### Components

  Check if you have all the components as in the image.
  <p align="center">
    <img src="media/hw-components.png" alt="Table1: 18 Activities" width="80%" height="80%">        
  </p>


### Assembly

  1. We will first mount the RPLiDAR on top of the robot. You will need to loosen the six screws on top of the robot with a 1/16" hex key. Connect the LiDAR to the Jetson board through the USB converter. Refer to the image below:  
  <p align="center">
    <img src="media/hw-lidar.png" alt="Table1: 18 Activities" width="70%" height="70%">        
  </p>

  2. We will now slide in the battery on the rear side of the robot and connect the battery cable to the power board. Make sure that the battery is charged. 

  3. Attach the white 3D printed plastic cover pieces unto the side of the robot. You can adjust the tightness using the hex key.  

  4. Mount the RealSense camera to the body using the USB right angle cable and set screw. Connect the other side of the cable to the Jetson board similar to the image below. (The original design of the RealSense camera is mounted hanging from the screw hole; however, it is also possible to mount the camera on top of the screw hole to reduce the software load of flipping the image). 
  <p align="center">
    <img src="media/hw-realsense.png" alt="Table1: 18 Activities" width="60%" height="60%">        
  </p>

  5. Attach the three wheels on the using the hex key and set screws included in the box. Slide the wheels into the motor and tighten the set screws to attach the wheels to the robot. 

  6. [Optional] WI-FI will work without the antennas but it will be very slow, so it is highly recommended you use them when you need a more reliable signal. Simply screw them to the top of Triton on the golden screws next to the LED ring. Because the antennas are tall, they may block some LIDAR laser points, but should not have a major effect on SLAM or localization performance. 

  7. Turn on the robot using the switch on top of the robot. The LED on your robot will light on. There are several other LED indicators (voltage convertor, Jetson board, Arduino) that will light on (the locations are mentioned in the video for this section). We can now check the connectivity and see if the robot in assembled properly. 


### Powering the robot

1. Plug the black power adapter into the outlet. 

2. Get the long power cable (with black/red wires) and plug the side with the male barrel connector to the female barrel connector of the power adapter. 

3. Plug the side of the long power cable with the female Dean's plug to the Triton robot. 

4. Turn on the switch of the robot located at the top back (beside the LIDAR). 

<p align="center">
  <img src="media/hw-dcpower.png" alt="DC Power" width="100%" height="50%">        
</p>


### Battery

  A LiPo battery (3.3A, 14.8V) is used for the power source of Triton. We can charge the battery with charger included in the starter box as shown below.
  <p align="center">
    <img src="media/hw-battery.png" alt="Table1: 18 Activities" width="50%" height="50%">        
  </p>

#### Charging

1. Plug the black wire into the outlet and the other end into the blue charger

2. Plug the two circular ends of the black and red wire into the blue charger and plug the other pronged end into the battery. If the pronged end is plugged into the battery and the circular ends are not plugged in, DO NOT touch the black and red ends together, it could potentially be very dangerous and could potentially ruin the battery.

3. The left side of the blue charger's screen shows the Amps the charger is currently set to and the right side shows the Volts the charger is currently set to. You are now ready to begin the charging procedure. This can be done by following these steps:
    
    1. Press an hold the *Start* button until the Amps start blinking

    2. Press *Inc* or *Dec* until the Amps get to <b>3.3</b>
    
    3. Press *Start*. You should now see the Volts blinking

    4. Press *Inc* or *Dec* until the Volts get to <b>14.8</b>

    5. Press and hold *Start* until you see the screen "Battery Check"

    6. When the screen says "Confirm", press *Start*. The battery should now start charging. The Volts in the top right corner represent the current charge of the battery. The battery is fully charged at <b>16.8</b> volts. DO NOT leave the battery unattended while it is charging.

    7. Once the battery is fully charged, it will beep and continue to beep until charging is stopped. You can stop it by pressing *Stop*

4. Once charging is complete, unplug the battery. It is now ready to be used on Triton!

This [video](https://youtube.com/shorts/HrVNtYbSuHs?feature=share) shows how to charge the battery but not in great detail.

#### Checking level

In order to check the battery level, unfortunately you have to go through all the steps above from the charging section (we are hoping to add a feature in the future that displays the battery's current volts or percentage physically on Triton or through ROS). Step vi is where you can see the battery's charge. As mentioned above, <b>16.8</b> volts is when the battery is full. <b>13.2</b> volts is when the battery is dead. DO NOT let the battery get to below <b>13.2</b> as puffing/bloating of the battery may occur (it has happened before).

If the battery bloats, DO NOT continue to use it. Report it to Professor Zhang as soon as possible.

Since bloating is a possiblity, and so Triton doesn't shut off on you out of the blue while you're working on it, unfortuantely you will have to check the battery's level relatively often. The battery life is empirically roughly <b>4-6</b> hours, so it is recommended you keep track of the time that the battery has been used and check the level every few hours or so.

### Check connectivity
  
  We will type in commands in the Jetson Nano board to check if our assembly is done properly.
  1. Place your robot somewhere safe (e.g., on the ground).

  2. Plug in a HDMI cable to a monitor, a keyboard, and a mouse to the Jetson. You will be able to see a screen with the Human-Centered Robotics Lab's logo. The password is <b>*triton*</b>.

  3. Open a terminal and run the `stingray_camera` `triton.launch` file:
  ``` bash
  cd catkin_ws
  source devel/setup.bash
  roslaunch stingray_camera triton.launch
  ```

  4. Open the third terminal and rostopic the following command:
  ``` bash
  rostopic pub /cmd_vel geometry_msgs/Twist "{linear: {x: 0.0, y: 0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 1.0}}" -r 10
  ``` 

  5. Your Triton is expected to rotate while having the motors to spin. We completed the connectivity check and your assembly is complete!
  <p align="center"> 
  <img src="media/triton-test.gif" alt="Sample signal" width="35%" height="35%">
  </p>

### Troubleshooting

If you're running into hardware issues, this [video](https://youtu.be/XCmTkdhDtzc) may help you diagonse and fix the problems.

If the hardware issues presist or are not covered in the video and you can't resolve them on your own, please reach out to the TA directly.



![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

<!-- SOFTWARE -->
<h2 id="software"> :large_orange_diamond: Software Setup</h2>

The base Jetson Nano runs Ubuntu <b>18.04</b> (the superuser password is <b>triton</b>) which only supports ROS <b>Melodic</b> (Where you checked the connenctivity). You can use ROS <b>Noetic</b> for your projects, the latest and final version of ROS1. As previously mentioned, ROS Noetic is in the Docker image (since the host OS doesn't support it), so you will need to run it through there. We will go through on how to set up and run the environment in this section.

<p align="justify"> 
  
### Working remotely

  You do not need to have physical connection with the robot to write code into the robot (although you can if you want to, simply plug in a keyboard and mouse). If you have access to a remote PC connected to the same network with the robot, you can directly *ssh* to the robot with the command `ssh triton@<IP-address>`. Take a look at other commands that are useful for transferring data: *rsync*, *scp*, *tar*, *zip*, *unzip*.

  It is possible to use Visual Studio Code (VSCode) from your remote PC to directly edit files on Triton using the Remote-SSH extension. The extension tunnels over SSH and you must set your `~/.ssh/config file` to let VSCode know the triton robot’s IP address. Please check [vscode remote](https://code.visualstudio.com/docs/remote/ssh) and [vscode ssh](https://code.visualstudio.com/docs/remote/ssh-tutorial) for further details.

  It can be inconvenient to create multiple virtual terminals within your docker. The *tmux* package can be useful for maintaining multiple terminals without worrying about disconnection or termination. Take a look at the [tutorial](https://github.com/tmux/tmux/wiki/Getting-Started) for details.

### Networking
  We can visualize the ROS networking as the diagram below. We assume that both Triton and your remote PC are connected to the same local network (via WiFi or ethernet) having the IP addresses: Triton (<b>192.168.0.30</b>), remote PC (<b>192.168.0.45</b>). You need to modify the addresses depending on your actual IP addresses assigned on your network​. On Triton you can run the `ifconfig` command. For Wi-Fi scroll down until you see "wlan0". On the line below, the number next to "inet" is Triton's current Wi-Fi IP address. See the terminal below.

  <p align="center"> 
    <img src="media/sw-ros.png" alt="Sample signal" width="65%" height="65%">
  </p>
  <p align="center"> 
    <img src="media/ip.png" alt="Triton Wi-Fi IP address" width="65%" height="65%">
  </p>
  
  To enable communication on ROS between the remote PC and Triton, it is important to set the *ROS_MASTER_URI* environment variable of the remote PC to the IP address of Triton and the ROS port <b>11311</b>. ​The additional environment variable *ROS_IP* helps Triton and the remote PC to discover each other on the network. This can be set on both the remote PC and Triton as shown in the diagram. On Triton, open the "docker-compose.yaml" file inside the Docker folder and set *ROS_MASTER_URI* and *ROS_IP* in the *environment* section (see the image below). On the remote PC, you can run `export ROS_IP=192.168.0.45` and `export ROS_MASTER_URI=http://192.168.0.30:11311` on bash (terminal). Add these commands to your `~/.bashrc` to automatically set this everytime you open your terminal. You can edit the *bashrc* by using the commands `vim ~/.bashrc` or `gedit ~/.bashrc`.

  <b>NOTE</b>: Even if you are not using remote coding, please set *ROS_MASTER_URI* and *ROS_IP* on Triton because those are needed for the Docker image to function properly.
  
  
  <p align="center"> 
    <img src="media/network_vars.png" alt="Docker network variables" width="65%" height="65%">
  </p>

### Running the `stingray_camera` package

For more information about running the package, check out the repository: https://gitlab.com/HCRLab/stingray-robotics/stingray_camera.git

One one terminal, run main launch file: 
```bash
cd ~/catkin_ws
source devel/setup.bash
roslaunch stingray_camera triton.launch
```

On another terminal, run the robot teleop:
```bash
cd ~/catkin_ws
source devel/setup.bash
rosrun stingray_camera teleop_robot.py
```


### Working with Docker

  We will first setup the config file to easily build a docker container and then load a docker image. We will then build the docker environment (container). For more information about running the pre-built image, check out the repository: https://gitlab.com/HCRLab/stingray-robotics/stingray_docker.git

  A sample docker image 

  1. Create a directory on Triton that contains the Docker image (e.g., triton_noetic_arm64.tar.gz) and the config file (e.g., docker-compose.yaml). YThe config file enables us to build a docker container in a more managable and convenient way (run docker-compose up).
  
  2. Add the *environment* section if it does not already exist. Set the *ROS_MASTER_URI* and *ROS_IP* in the *environment* section as shown above. 

  3. You can mount volumes (e.g., utilize your host directories) to the Docker container by adding the *volumes* field in the config file. For example, we mount the child directory named `files` (`${PWD}/files`) to the docker container (`/files`) in the sample config file shown below. We also mount the directory `rospackage` to the docker container at `/catkin_ws/src/rospackage`. ​If you prefer to copy files instead of mounting volumes, you can use the command `docker cp <src_path> <container_id>:<dest_path>` (e.g., `docker cp files c1e94410eedc:/files`). Use the command `docker ps` to check the *container_id*. ​Here is an example:

  <p align="center"> 
    <img src="media/mount.png" alt="Mounting folders into Docker" width="65%" height="65%">
  </p>

  4. Load the docker image using the compressed file from the directory we just created (this may take some time). Check if the image is loaded by the command: `docker image list`.
  ``` bash
  run docker load –input triton_noetic.tar.gz
  ```  

  5. Before builing the docker container, verify if *docker-compose* is installed on your system with the command `which docker-compose`. Take a look at the [instructions](https://collabnix.com/running-docker-compose-on-nvidia-jetson-nano-in-5-minutes/) if it is not installed. If *docker-compose* is still not accessible after following the installation steps, make sure that `$HOME/.local/bin` is in your `PATH`. You can add this to you path by adding the following line to your `~/.bashrc`.
  ``` bash
  export PATH=~/.local/bin:$PATH
  ```

  6. Run the docker container using the first command. If you get an error, sometimes running the second command fixes it.
  ``` bash
  docker-compose up
  ```
  ``` bash
  sudo docker-compose up
  ```

  7. You can now follow the steps in this [video](https://youtu.be/MpRGMVvnzUg) to learn how to interact with the ROS topics (LiDAR, camera, etc.). The video demonstrates the ROS topics through ROS Melodic on the host OS (outside the Docker container). This is fine since the same topics are exposed to Melodic and Noetic. If you want to see the camera feed or use RViz in general, you actually have to do that through ROS Melodic on the host OS since this Docker image doesn't support launching GUIs. For a small guide see the RViz section below.

  8. To open a terminal within the Docker container so that you can work with ROS Noetic, open another terminal and execute this bash command inside the directory containing the `docker-compose.yaml` config file​. In this way, you can directly run `roslaunch` and `rosrun` commands, as well as install other Python or ROS dependencies. 
   ``` bash
  docker-compose exec triton_noetic bash
  ```

  9. To stop the Docker container press *CTRL+C* in the terminal where you ran `docker-compose up`

  * Additional notes
    * If you see any errors such as `The requested device is NOT found` or `Device 1/1 failed with exception: failed to set power state`, just stop the docker-compose with CTRL+C and run docker-compose up again​.
    * Inside the bash terminal, you can directly run commands such as *apt*, *pip*, *wget*, *git clone*, etc. ​
    * If a Linux command is not available (e.g., *vim*), you can install it directly with `apt install vim`. ​
​
### RViz
After the Docker is built, launch the docker container.
``` bash
  docker-compose up
```

Open a docker terminal: 

``` bash
docker-compose exec triton_noetic bash
```


This should run the lidar, camera, and drive command code inside the container. 

### Flipping the camera
Due to the way the camera is physically mounted on Triton (if upside down), the feed is flipped vertically. To correct it, you need to flip the feed so that it looks normal. You can do that multiple ways.

Option 1 [Recommended] - Flipping the feed directly

This [ROS package](https://github.com/ros-visualization/rqt_image_view/blob/master/src/rqt_image_view/image_view.cpp) can be used to change various parameters of a camera, including flipping the image. 

Option 2 - Flipping the frames during preprocessing

You can preprocess the frames in Python before you work them using the vision/image library of your choice (i.e. OpenCV) to flip the frames vertically. Please note that choosing this option means your original feed will still be flipped, thus it will look flipped on RViz, which might be annoying or limiting during testing.

![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

<h2 id="Poweroff"> :large_orange_diamond: Powering Off</h2>

In order to power off Triton, save all of your work, kill the Docker container with *CTRL+C* (you may need to press it multiple times), and then shut down Ubuntu. Wait until all of the text on the black screen disappears then flip the switch on the top of Triton. Triton is now fully powered off.


![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/rainbow.png)

✤ <i>This page supports the course <a href="https://hcr.cs.umass.edu/courses/compsci603/">COMPSCI 603 - Robotics</a>, at <a href="https://www.umass.edu/">University of Massachusetts Amherst</a><i>